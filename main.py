import gzip
from typing import Optional

import nanoid
import redis
from fastapi import FastAPI, Form
from fastapi.responses import PlainTextResponse, RedirectResponse
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    redis_host: str = "127.0.0.1"
    redis_port: int = 6379


cfg = Settings()
app = FastAPI()


redis_conn = redis.Redis(
    host=cfg.redis_host, port=cfg.redis_port, decode_responses=False
)


def generate_id():
    alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    size = 9
    return nanoid.generate(alphabet, size)


@app.get("/")
def redirect_to_gitlab():
    return RedirectResponse("https://gitlab.gnome.org/explore/snippets")


@app.get("/status")
def healthcheck():
    return {"status": "OK"}


@app.get("/api/json/parameter/expire")
def get_parameter_expire():
    return {"result": {"values": [1800, 21600, 86400, 604800, 2592000]}}


@app.post("/api/json/create")
def create_paste(
    data: str = Form(...),
    language: str = Form("text"),
    title: Optional[str] = Form(None),
    expire: Optional[int] = Form(None),
):
    paste_id = generate_id()
    content = gzip.compress(data.encode())
    redis_conn.setex(paste_id, 24 * 60 * 60, content)
    return {"result": {"id": paste_id}}


@app.get("/{paste_id}")
def get_paste(paste_id: str):
    if paste := redis_conn.get(paste_id):
        content = gzip.decompress(paste)
        return PlainTextResponse(content=content)

    return PlainTextResponse("paste not found", status_code=404)
